# easy arm xross gcc (eax-gcc) #

(c) 2013-2015 Daniel Zimmermann

A makefile project that build a gcc cross compiler for arm and nios2
The gcc compiler will run under Linux and Windows


## Makefile commands ##

1.) Clean all 

```make
make cleanall
```

2.) Clean all except the downloaded file

```make
make clean
```

3.) Build all

```make
make arch=arm|nios2 ct_ng_version=trunk all 
```

4.) Build all with menuconfig for ct-ng

```make
make arch=arm|nios2 ct_ng_version=trunk all_with_menuconfig
```

5.) Show help to the Makefile

```make
make help
```

